package com.test.demo.service;

import com.test.demo.model.Application;
import com.test.demo.model.ApplicationDTO;

import java.util.List;
import java.util.Map;


public interface ApplicationService {
    void addNewApplicationWithTemplate(Application application);
    Application findByIdTemplate(String id);
    void deleteWithTemplate(Application application);
    void updateApplicationWithTemplate(Application application);
    Map<String, Object> getApplicationCountUsingAggregation();
    List<ApplicationDTO> getOwnerAboveTwoCountUsingAggregation();
    List<Application> getDetailsBasedOnNameUsingQuery(String name);
    void retireApplication(Application application);
}
