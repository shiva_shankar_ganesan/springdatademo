package com.test.demo.service;

import com.test.demo.model.Application;
import com.test.demo.model.ApplicationDTO;
import com.test.demo.model.Ticket;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public void addNewApplicationWithTemplate(Application application) {
        mongoTemplate.insert(application);
    }

    @Override
    public Application findByIdTemplate(String id){
        return mongoTemplate.findById(id, Application.class);
    }

    @Override
    public void deleteWithTemplate(Application application){
        mongoTemplate.remove(application);
    }

    @Override
    public void updateApplicationWithTemplate(Application application){
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(application.getName()));
        Update update = new Update();
        update.set("name", "Test");
        mongoTemplate.updateFirst(query, update, Application.class);
    }

    @Override
    public Map<String, Object> getApplicationCountUsingAggregation() {

        Aggregation aggregation= Aggregation.newAggregation(Aggregation.group().count().as("count"));
        return mongoTemplate.aggregate(aggregation, "application", Application.class).getRawResults();
    }

    @Override
    public List<ApplicationDTO> getOwnerAboveTwoCountUsingAggregation(){

        /*GroupOperation groupOperation = Aggregation.group("owner").count().as("ownerCount");
        MatchOperation matchOperation = Aggregation.match(new Criteria("ownerCount").gt(2));
        SortOperation sortOperation = Aggregation.sort(Sort.Direction.ASC, "ownerCount");
        ProjectionOperation projectionOperation = Aggregation.project().and("_id").as("name").and("ownerCount").as("counts").andExclude("_id");
        Aggregation aggregation = Aggregation.newAggregation(groupOperation, matchOperation, sortOperation, projectionOperation);
        return mongoTemplate.aggregate(aggregation, "application", Document.class).getRawResults();*/
        System.out.println("Testing");
        MatchOperation matchOperation = Aggregation.match(new Criteria("owner").is("Shiva"));
        GroupOperation groupOperation = Aggregation.group("owner").count().as("ownerCount");
        ProjectionOperation projectionOperation = Aggregation.project().and("_id").as("owner").and("ownerCount").as("count").andExclude("_id");
        Aggregation pipeline = Aggregation.newAggregation(matchOperation,groupOperation,projectionOperation);
        AggregationResults results = mongoTemplate.aggregate(pipeline,Application.class, ApplicationDTO.class);
        List<ApplicationDTO> applications = results.getMappedResults();
        return applications;
    }

    public List<Application> getDetailsBasedOnNameUsingQuery(String name){
        Query nameQuery = new Query();
        nameQuery.addCriteria(Criteria.where("name").is(name));
        List<Application> applicationList = mongoTemplate.find(nameQuery, Application.class);
        return applicationList;
    }

    @Override
    @Transactional
    public void retireApplication(Application application) {
        mongoTemplate.remove(application);
        Query query = new Query();
        query.addCriteria(Criteria.where("appId").is(application.getId()));
        Update update = new Update();
        update.set("status", "Cancel");
        mongoTemplate.updateMulti(query, update, Ticket.class);
    }

}
