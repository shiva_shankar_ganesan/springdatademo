package com.test.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataMongoDemo {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataMongoDemo.class, args);
    }

}
