package com.test.demo.controller;

import com.test.demo.model.*;
import com.test.demo.repositories.ApplicationRepository;
import com.test.demo.repositories.TicketRepository;
import com.test.demo.service.ApplicationService;
import com.test.demo.service.ReleaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

@RestController
public class TestController {

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private ReleaseService releaseService;

    //************ Methods for User Session****************************
    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    public User addUserToSession(@RequestBody User user, HttpSession session) {
        session.setAttribute("USER", user);
        return user;
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Object getUserFromSession(HttpSession session){
        return session.getAttribute("USER");
    }


    //************* Transaction Management *****************************
    @RequestMapping(value = "retire/application", method = RequestMethod.DELETE)
    public void retireApplication(@RequestBody Application application) {  //just pass in the Id
        applicationService.retireApplication(application);
    }

    @GetMapping(value = "/applications")
    public List<Application> getAllApplications() {
        return applicationRepository.findAll();
    }

    @GetMapping(value = "/applications/count")
    public Map<String, Object> getApplicationCount(){
        return  applicationService.getApplicationCountUsingAggregation();
    }

    @GetMapping(value = "/application/ownerCount")
    public List<ApplicationDTO> getBasedOwnerCount(){
        return applicationService.getOwnerAboveTwoCountUsingAggregation();
    }

    @GetMapping(value =  "/applications/findByName/{name}")
    public List<Application> getBasedonName(@PathVariable("name") String name){
        return applicationService.getDetailsBasedOnNameUsingQuery(name);
    }


    @GetMapping( value = "/applications/{id}")
    public Optional<Application> getApplicationById(@PathVariable("id") String id) {
        return applicationRepository.findById(id);
    }

    @PostMapping(value = "/applications")
    public Application addNewApplication(@RequestBody Application application){
        return applicationRepository.save(application);
    }

    @PutMapping(value = "/applications/{id}")
    public Application updateApplication(@PathVariable("id") String id, @RequestBody Application application){
        application.setId(id);
        return applicationRepository.save(application);
    }

    @DeleteMapping(value = "/applications/{id}")
    public void deleteApplication(@PathVariable("id") String id) {
        applicationRepository.deleteById(id);
    }

    @GetMapping(value = "/applications/name/{name}")
    public List<Application> findByName(@PathVariable("name") String name) {
        return applicationRepository.findByName(name);
    }

    //MongoTemplate Methods
    @PostMapping(value = "/applications/template")
    public void addNewApplicationWTemplate(@RequestBody Application application){
        applicationService.addNewApplicationWithTemplate(application);
    }

    @GetMapping(value = "/applications/template/name/{id}")
    public Application findByIdTemplate(@PathVariable("id") String id){
        return applicationService.findByIdTemplate(id);
    }

    @DeleteMapping(value = "/applications/template")
    public void deleteWTemplate(@RequestBody Application application){
        applicationService.deleteWithTemplate(application);
    }

    @PutMapping(value = "/applications/template")
    public void updateApplicationWTemplate(@RequestBody Application application){
        applicationService.updateApplicationWithTemplate(application);
    }

    // ************** Methods for Tickets *************************
    @GetMapping(value = "/tickets")
    public List<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }

    @GetMapping(value = "/tickets/{id}")
    public Optional<Ticket> getTicketById(@PathVariable("id") String id) {
        return ticketRepository.findById(id);
    }

    @PostMapping(value = "/tickets")
    public Ticket addNewTicket(@RequestBody Ticket ticket){
        return ticketRepository.save(ticket);
    }

    @PutMapping(value = "/tickets/{id}")
    public Ticket updateTickets(@PathVariable("id") String id, @RequestBody Ticket ticket){
        ticket.setId(id);
        return ticketRepository.save(ticket);
    }

    @DeleteMapping(value = "/tickets/{id}")
    public void deleteTicket(@PathVariable("id") String id) {
        ticketRepository.deleteById(id);
    }

    @GetMapping(value = "/tickets/status/{status}")
    public List<Ticket> findByStatus(@PathVariable("status") String status) {
        return ticketRepository.findByStatus(status);
    }

    @GetMapping(value = "/tickets/count")
    public Long countAllTickets() {
        Stream<Ticket> stream = ticketRepository.findAllByCustomQueryAndStream("Closed");
        Long count = stream.count();
        stream.close();
        return count;
    }

    @GetMapping(value = "/tickets/appId/{id}")
    public List<Ticket> findByApplicationId(@PathVariable("id") String appId) {
        return ticketRepository.findByAppId(appId);
    }


    // ************** Methods for Releases *************************
    @GetMapping(value = "/releases")
    public List<Release> getAllReleases() {
        return releaseService.findAll();
    }

    @GetMapping(value = "/releases/{id}")
    public Optional<Release> getReleaseId(@PathVariable("id") String id) {
        return releaseService.findById(id);
    }

    @PostMapping(value = "/releases")
    public Release addNewRelease(@RequestBody Release release){
        return releaseService.save(release);
    }

    @PutMapping(value = "/releases/{id}")
    public Release updateRelease(@PathVariable("id") String id, @RequestBody Release release){
        release.setId(id);
        return releaseService.save(release);
    }

    @DeleteMapping(value = "/releases/{id}")
    public void deleteRelease(@PathVariable("id") String id) {
        releaseService.deleteById(id);
    }

    @PutMapping(value ="/releases/tickets")
    public void addNewReleaseWTickets(@RequestBody Release release) {
        releaseService.insert(release);
    }

    @GetMapping(value = "/releases/status/{status}")
    public List<Release> getReleaseByTicketStatus(@PathVariable("status") String status){
        return releaseService.getReleaseByTicketStatus(status);
    }

    @GetMapping(value = "/releases/costs/{id}")
    public Double getReleaseCost(@PathVariable("id") String id) {
        return releaseService.getCosts(id);
    }


}
