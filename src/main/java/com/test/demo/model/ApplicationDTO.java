package com.test.demo.model;

public class ApplicationDTO {

    private String owner;
    private int count;

    public ApplicationDTO() {
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "ApplicationDTO{" +
                "owner='" + owner + '\'' +
                ", count=" + count +
                '}';
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
